FROM registry.gitlab.com/andrewheberle/opencv-builder:4.5.2 AS opencv

FROM registry.gitlab.com/andrewheberle/docker-remco:0.12.1-ubuntu

ENV MLAPI_VERSION="2.2.18" \
    TZ="Etc/UTC"

ARG BUILD_DEPS="curl wget make gcc cmake clang libev-dev python3-pip python3-dev python3-venv"
ARG RUNTIME_DEPS="python3 python3-crypto python3-cryptography python3-distutils libev4 zlib1g"

# Copy opencv
COPY --from=opencv /opencv/ /usr/

# Install deps
RUN apt-get -qqy update && \
    apt-get -qqy install --no-install-recommends ${BUILD_DEPS} && \
    apt-get -qqy install ${RUNTIME_DEPS} && \
    mkdir -p /var/lib/zmeventnotification && \
    curl -L --silent "https://github.com/pliablepixels/mlapi/archive/refs/tags/v${MLAPI_VERSION}.tar.gz" | \
        tar -C /tmp -zxf - && \
    mv "/tmp/mlapi-${MLAPI_VERSION}" /var/lib/zmeventnotification/mlapi && \
    python3 -m venv --system-site-packages /opt/mlapi && \
    . /opt/mlapi/bin/activate && \
    cd /var/lib/zmeventnotification/mlapi && \
    pip3 install -r requirements.txt && \
    cd /var/lib/zmeventnotification && ./mlapi/get_models.sh && \
    mv mlapi/images mlapi/known_faces mlapi/unknown_faces . && \
    useradd -m -s /usr/sbin/nologin -d /nonexistent mlapi && \
    apt-get -qqy purge ${BUILD_DEPS} && \
    apt-get -qqy auto-remove && \
    apt-get -qqy clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /root/.cache

ENV ZM_MLAPI_USER="mluser" \
    ZM_MLAPI_PASSWORD="mlpassword" \
    VIRTUAL_ENV="/opt/mlapi" \
    PATH="/opt/mlapi/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

COPY rootfs /

RUN mkdir -p /etc/zm

EXPOSE 5000
