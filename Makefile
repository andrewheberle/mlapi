TAG ?= latest
CACHE_TAG ?= latest

ifndef IMAGE
$(error IMAGE must be provided)
endif

all: latest tag

tag: docker-build docker-push-tag

latest: docker-build docker-push

docker-build:
	docker build --cache-from $(IMAGE):$(CACHE_TAG) -t $(IMAGE) --build-arg BUILDKIT_INLINE_CACHE=1 .
	touch $@

docker-tag: docker-build
	docker tag "$(IMAGE)" "$(IMAGE):$(TAG)"
	touch $@

docker-push-tag: docker-build docker-tag
	docker push "$(IMAGE):$(TAG)"
	touch $@

docker-push: docker-build
	docker push "$(IMAGE)"
	touch $@

clean:
	$(RM) docker-build docker-tag docker-push docker-push-tag

.PHONY: all tag latest clean